#!/bin/bash
echo $@

# sed vlan number in users file
if [ -n $GUESTVLAN ]; then
  sed -i "s/Tunnel-Private-Group-Id =.*$/Tunnel-Private-Group-Id = ${GUESTVLAN}/g" /etc/freeradius/mods-config/files/authorize
fi

# start freeradius service
/bin/bash -c "freeradius -f $1"
